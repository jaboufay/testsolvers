package checksafety;

import java.util.HashMap;
import java.util.LinkedList;

import fr.kairos.lightccsl.core.stepper.INameToIntegerMapper;

public class MyNameMapper implements INameToIntegerMapper {
	private LinkedList<String> names = new LinkedList<>();
	private HashMap<String,Integer> nameToId = new HashMap<>();
	
	@Override
	public Iterable<String> getClockNames() {
		return names;
	}
	@Override
	public int getIdFromName(String name) {
		Integer v = nameToId.get(name);
		if (v == null) {
			nameToId.put(name, v = names.size());
			names.add(name);
		}
		return v;
	}
	@Override
	public String getNameFromId(int arg0) {
		return names.get(arg0);
	}
	@Override
	public int size() {
		return names.size();
	}
	
	
}
