package checksafety;
import java.util.Arrays;

import fr.kairos.timesquare.ccsl.sat.IBooleanSpecification;

public class Sat4jRules{
	
	static public void build_Equiv(IBooleanSpecification spec, String a, String b, String c) {
		spec.clause(a,b,c);
		spec.clause(b,a,c);
		spec.implies(a,c);
		spec.or(b+c,b,c);
		spec.forces(b+c);
	}

//	static public void build_Xor(IBooleanSpecification spec, String a, String b, String c) {
//		spec.clause(c,a,b);
//		spec.clause(b,a,c);
//		spec.clause(a,b,c);
//	}
	
	static public void build_goal1_cond1(IBooleanSpecification spec) {
		//spec.not("goal1_cond1");
		spec.or("goal1_cond1", "goal1_cond1_1", "goal1_cond1_2");
		build_Equiv(spec,"goal1_cond1_1", "not_exist_front_car_distance",
					"goal1_cond1_1_1");
		spec.or("goal1_cond1_1_1", "not_exist_front_car_tracking",
				"disappeared_less_than_t1_front_car_tracking", 
				"disappeared_more_than_t1_front_car_tracking");
		build_Equiv(spec,"goal1_cond1_2", "not_exist_straddling_car_distance",
					"not_exist_straddling_car_tracking");
	}

	static public void build_goal2_cond1(IBooleanSpecification spec) {
		spec.and("goal2_cond1", "stable_stable_control", "goal2_cond1_1");
		spec.or("goal2_cond1_1", 
				"disappeared_more_than_t1_front_car_tracking", 
				"no_detection_in_more_than_t6_line_detection", 
				"straddling_more_than_t7_straddling_car_tracking");
	}
	
	static public void build_goal3_cond1(IBooleanSpecification spec) {
		spec.or("goal3_cond1", "imminent_collision_distance_front_car_distance");
	}
	
	static public void build_goal3_cond2(IBooleanSpecification spec) {
		spec.or("goal3_cond2", "imminent_collision_distance_straddling_car_distance");
	}
	
	static public void build_goal4_cond1(IBooleanSpecification spec) {
		spec.or("goal4_cond1", "imminent_collision_distance_front_car_distance", "imminent_collision_distance_straddling_car_distance");
	}
	
	static public void build_goal4_cond2(IBooleanSpecification spec) {
		spec.or("goal4_cond2", "not_confirmed_front_car_tracking");
	}
	
	static public void build_goal4_cond3(IBooleanSpecification spec) {
		spec.or("goal4_cond3", "emergency_distance_rear_car_distance");
	}
	
	static public void build_goal4_cond4(IBooleanSpecification spec) {
		spec.or("goal4_cond4", "strong_braking_distance_front_car_distance", "strong_braking_distance_straddling_car_distance");
	}
	
	static public void build_goal4_cond5(IBooleanSpecification spec) {
		spec.or("goal4_cond5", "acc_distance_front_car_distance", "acc_distance_straddling_car_distance");
	}
	
} 
