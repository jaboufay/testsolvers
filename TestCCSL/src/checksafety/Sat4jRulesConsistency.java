package checksafety;
import fr.kairos.timesquare.ccsl.sat.IBooleanSpecification;

public class Sat4jRulesConsistency{

//Creating function *build_goal(x)_condition(y)_True*
//Creating function *build_goal(x)_condition(y)_False*
//Creating function *build_goal(x)_condition(y)_Alert*
//Creating function *build_goal(x)_condition(y)_Alert_True*
//Creating function *build_goal(x)_condition(y)_Alert_False*

	static public void build_goal1_cond1_True(IBooleanSpecification spec) {
		Sat4jRules.build_goal1_cond1(spec);
		spec.forces("goal1_cond1");
	}
	
	static public void build_goal1_cond1_False(IBooleanSpecification spec) {
		Sat4jRules.build_goal1_cond1(spec);
		spec.not("goal1_cond1");
	}
	
	static public void build_goal1_cond1_Alert(IBooleanSpecification spec) {
		Sat4jRules.build_goal1_cond1(spec);
		spec.and("goal1_cond1_Alert", "goal1_cond1","bug_notification");
	}
	
	static public void build_goal1_cond1_Alert_True(IBooleanSpecification spec) {
		build_goal1_cond1_Alert(spec);
		spec.forces("goal1_cond1_Alert");
	}
	
	static public void build_goal1_cond1_Alert_False(IBooleanSpecification spec) {
		build_goal1_cond1_Alert(spec);
		spec.not("goal1_cond1_Alert");
	}
	
	static public void build_goal2_cond1_True(IBooleanSpecification spec) {
		Sat4jRules.build_goal2_cond1(spec);
		spec.forces("goal2_cond1");
	}
	
	static public void build_goal2_cond1_False(IBooleanSpecification spec) {
		Sat4jRules.build_goal2_cond1(spec);
		spec.not("goal2_cond1");
	}
	
	static public void build_goal2_cond1_Alert(IBooleanSpecification spec) {
		Sat4jRules.build_goal2_cond1(spec);
		spec.and("goal2_cond1_Alert", "goal2_cond1","eop1");
	}
	
	static public void build_goal2_cond1_Alert_True(IBooleanSpecification spec) {
		build_goal2_cond1_Alert(spec);
		spec.forces("goal2_cond1_Alert");
	}
	
	static public void build_goal2_cond1_Alert_False(IBooleanSpecification spec) {
		build_goal2_cond1_Alert(spec);
		spec.not("goal2_cond1_Alert");
	}
	
	static public void build_goal3_cond1_True(IBooleanSpecification spec) {
		Sat4jRules.build_goal3_cond1(spec);
		spec.forces("goal3_cond1");
	}
	
	static public void build_goal3_cond1_False(IBooleanSpecification spec) {
		Sat4jRules.build_goal3_cond1(spec);
		spec.not("goal3_cond1");
	}
	
	static public void build_goal3_cond1_Alert(IBooleanSpecification spec) {
		Sat4jRules.build_goal3_cond1(spec);
		spec.and("goal3_cond1_Alert", "goal3_cond1","longitudinal_acceleration");
	}
	
	static public void build_goal3_cond1_Alert_True(IBooleanSpecification spec) {
		build_goal3_cond1_Alert(spec);
		spec.forces("goal3_cond1_Alert");
	}
	
	static public void build_goal3_cond1_Alert_False(IBooleanSpecification spec) {
		build_goal3_cond1_Alert(spec);
		spec.not("goal3_cond1_Alert");
	}
	static public void build_goal3_cond2_True(IBooleanSpecification spec) {
		Sat4jRules.build_goal3_cond2(spec);
		spec.forces("goal3_cond2");
	}
	
	static public void build_goal3_cond2_False(IBooleanSpecification spec) {
		Sat4jRules.build_goal3_cond2(spec);
		spec.not("goal3_cond2");
	}
	
	static public void build_goal3_cond2_Alert(IBooleanSpecification spec) {
		Sat4jRules.build_goal3_cond2(spec);
		spec.and("goal3_cond2_Alert", "goal3_cond2","longitudinal_acceleration");
	}
	
	static public void build_goal3_cond2_Alert_True(IBooleanSpecification spec) {
		build_goal3_cond2_Alert(spec);
		spec.forces("goal3_cond2_Alert");
	}
	
	static public void build_goal3_cond2_Alert_False(IBooleanSpecification spec) {
		build_goal3_cond2_Alert(spec);
		spec.not("goal3_cond2_Alert");
	}
	
	static public void build_goal4_cond1_True(IBooleanSpecification spec) {
		Sat4jRules.build_goal4_cond1(spec);
		spec.forces("goal4_cond1");
	}
	
	static public void build_goal4_cond1_False(IBooleanSpecification spec) {
		Sat4jRules.build_goal4_cond1(spec);
		spec.not("goal4_cond1");
	}
	
	static public void build_goal4_cond1_Alert(IBooleanSpecification spec) {
		Sat4jRules.build_goal4_cond1(spec);
		spec.and("goal4_cond1_Alert", "goal4_cond1","longitudinal_acceleration");
	}
	
	static public void build_goal4_cond1_Alert_True(IBooleanSpecification spec) {
		build_goal4_cond1_Alert(spec);
		spec.forces("goal4_cond1_Alert");
	}
	
	static public void build_goal4_cond1_Alert_False(IBooleanSpecification spec) {
		build_goal4_cond1_Alert(spec);
		spec.not("goal4_cond1_Alert");
	}
	static public void build_goal4_cond2_True(IBooleanSpecification spec) {
		Sat4jRules.build_goal4_cond2(spec);
		spec.forces("goal4_cond2");
	}
	
	static public void build_goal4_cond2_False(IBooleanSpecification spec) {
		Sat4jRules.build_goal4_cond2(spec);
		spec.not("goal4_cond2");
	}
	
	static public void build_goal4_cond2_Alert(IBooleanSpecification spec) {
		Sat4jRules.build_goal4_cond2(spec);
		spec.and("goal4_cond2_Alert", "goal4_cond2","longitudinal_acceleration");
	}
	
	static public void build_goal4_cond2_Alert_True(IBooleanSpecification spec) {
		build_goal4_cond2_Alert(spec);
		spec.forces("goal4_cond2_Alert");
	}
	
	static public void build_goal4_cond2_Alert_False(IBooleanSpecification spec) {
		build_goal4_cond2_Alert(spec);
		spec.not("goal4_cond2_Alert");
	}
	static public void build_goal4_cond3_True(IBooleanSpecification spec) {
		Sat4jRules.build_goal4_cond3(spec);
		spec.forces("goal4_cond3");
	}
	
	static public void build_goal4_cond3_False(IBooleanSpecification spec) {
		Sat4jRules.build_goal4_cond3(spec);
		spec.not("goal4_cond3");
	}
	
	static public void build_goal4_cond3_Alert(IBooleanSpecification spec) {
		Sat4jRules.build_goal4_cond3(spec);
		spec.and("goal4_cond3_Alert", "goal4_cond3","no_alert_needed");
	}
	
	static public void build_goal4_cond3_Alert_True(IBooleanSpecification spec) {
		build_goal4_cond3_Alert(spec);
		spec.forces("goal4_cond3_Alert");
	}
	
	static public void build_goal4_cond3_Alert_False(IBooleanSpecification spec) {
		build_goal4_cond3_Alert(spec);
		spec.not("goal4_cond3_Alert");
	}
	static public void build_goal4_cond4_True(IBooleanSpecification spec) {
		Sat4jRules.build_goal4_cond4(spec);
		spec.forces("goal4_cond4");
	}
	
	static public void build_goal4_cond4_False(IBooleanSpecification spec) {
		Sat4jRules.build_goal4_cond4(spec);
		spec.not("goal4_cond4");
	}
	
	static public void build_goal4_cond4_Alert(IBooleanSpecification spec) {
		Sat4jRules.build_goal4_cond4(spec);
		spec.and("goal4_cond4_Alert", "goal4_cond4","longitudinal_acceleration");
	}
	
	static public void build_goal4_cond4_Alert_True(IBooleanSpecification spec) {
		build_goal4_cond4_Alert(spec);
		spec.forces("goal4_cond4_Alert");
	}
	
	static public void build_goal4_cond4_Alert_False(IBooleanSpecification spec) {
		build_goal4_cond4_Alert(spec);
		spec.not("goal4_cond4_Alert");
	}
	static public void build_goal4_cond5_True(IBooleanSpecification spec) {
		Sat4jRules.build_goal4_cond5(spec);
		spec.forces("goal4_cond5");
	}
	
	static public void build_goal4_cond5_False(IBooleanSpecification spec) {
		Sat4jRules.build_goal4_cond5(spec);
		spec.not("goal4_cond5");
	}
	
	static public void build_goal4_cond5_Alert(IBooleanSpecification spec) {
		Sat4jRules.build_goal4_cond5(spec);
		spec.and("goal4_cond5_Alert", "goal4_cond5","longitudinal_acceleration");
	}
	
	static public void build_goal4_cond5_Alert_True(IBooleanSpecification spec) {
		build_goal4_cond5_Alert(spec);
		spec.forces("goal4_cond5_Alert");
	}
	
	static public void build_goal4_cond5_Alert_False(IBooleanSpecification spec) {
		build_goal4_cond5_Alert(spec);
		spec.not("goal4_cond5_Alert");
	}
	
//test priority rules by applying : test(cond1 and !cond2) ou test(!cond1 and cond2)
	
	
//	static public void testPrioritygoal3_cond2_over_goal3_cond1(IBooleanSpecification spec) {
//		build_goal3_cond2_True(spec);
//		build_goal3_cond1_False(spec);
//	}
//	
//	static public void testPrioritygoal3_cond1_over_goal3_cond2(IBooleanSpecification spec) {
//		build_goal3_cond2_False(spec);
//		build_goal3_cond1_True(spec);
//	}
//	
//	static public void testPrioritygoal3_cond2_over_goal3_cond1_Alerts(IBooleanSpecification spec) {
//		build_goal3_cond2_Alert_True(spec);
//		build_goal3_cond1_Alert_False(spec);
//	}
//	
//	static public void testPrioritygoal3_cond1_over_goal3_cond2_Alerts(IBooleanSpecification spec) {
//		build_goal3_cond2_Alert_False(spec);
//		build_goal3_cond1_Alert_True(spec);
//	}
//	
	
	static public void testPrioritygoal4_cond2_over_goal4_cond1(IBooleanSpecification spec) {
		build_goal4_cond2_True(spec);
		build_goal4_cond1_False(spec);
	}
	
//	static public void testPrioritygoal4_cond1_over_goal4_cond2(IBooleanSpecification spec) {
//		build_goal4_cond2_False(spec);
//		build_goal4_cond1_True(spec);
//	}
	
	static public void testPrioritygoal4_cond2_over_goal4_cond1_Alerts(IBooleanSpecification spec) {
		build_goal4_cond2_Alert_True(spec);
		build_goal4_cond1_Alert_False(spec);
	}
	
//	static public void testPrioritygoal4_cond1_over_goal4_cond2_Alerts(IBooleanSpecification spec) {
//		build_goal4_cond2_Alert_False(spec);
//		build_goal4_cond1_Alert_True(spec);
//	}
	
	static public void testPrioritygoal4_cond3_over_goal4_cond2(IBooleanSpecification spec) {
		build_goal4_cond3_True(spec);
		build_goal4_cond2_False(spec);
	}
	
//	static public void testPrioritygoal4_cond2_over_goal4_cond3(IBooleanSpecification spec) {
//		build_goal4_cond3_False(spec);
//		build_goal4_cond2_True(spec);
//	}
	
	static public void testPrioritygoal4_cond3_over_goal4_cond2_Alerts(IBooleanSpecification spec) {
		build_goal4_cond3_Alert_True(spec);
		build_goal4_cond2_Alert_False(spec);
	}
	
//	static public void testPrioritygoal4_cond2_over_goal4_cond3_Alerts(IBooleanSpecification spec) {
//		build_goal4_cond3_Alert_False(spec);
//		build_goal4_cond2_Alert_True(spec);
//	}
	
	static public void testPrioritygoal4_cond4_over_goal4_cond3(IBooleanSpecification spec) {
		build_goal4_cond4_True(spec);
		build_goal4_cond3_False(spec);
	}
	
//	static public void testPrioritygoal4_cond3_over_goal4_cond4(IBooleanSpecification spec) {
//		build_goal4_cond4_False(spec);
//		build_goal4_cond3_True(spec);
//	}
	
	static public void testPrioritygoal4_cond4_over_goal4_cond3_Alerts(IBooleanSpecification spec) {
		build_goal4_cond4_Alert_True(spec);
		build_goal4_cond3_Alert_False(spec);
	}
	
//	static public void testPrioritygoal4_cond3_over_goal4_cond4_Alerts(IBooleanSpecification spec) {
//		build_goal4_cond4_Alert_False(spec);
//		build_goal4_cond3_Alert_True(spec);
//	}
	
	static public void testPrioritygoal4_cond5_over_goal4_cond4(IBooleanSpecification spec) {
		build_goal4_cond5_True(spec);
		build_goal4_cond4_False(spec);
	}
	
//	static public void testPrioritygoal4_cond4_over_goal4_cond5(IBooleanSpecification spec) {
//		build_goal4_cond5_False(spec);
//		build_goal4_cond4_True(spec);
//	}
	
	static public void testPrioritygoal4_cond5_over_goal4_cond4_Alerts(IBooleanSpecification spec) {
		build_goal4_cond5_Alert_True(spec);
		build_goal4_cond4_Alert_False(spec);
	}
	
//	static public void testPrioritygoal4_cond4_over_goal4_cond5_Alerts(IBooleanSpecification spec) {
//		build_goal4_cond5_Alert_False(spec);
//		build_goal4_cond4_Alert_True(spec);
//	}
	
	
//	static public void testPrioritygoal1_cond1_over_goal2_cond1(IBooleanSpecification spec) {
//		build_goal1_cond1_True(spec);
//		build_goal2_cond1_False(spec);
//	}
	
	static public void testPrioritygoal2_cond1_over_goal1_cond1(IBooleanSpecification spec) {
		build_goal1_cond1_True(spec);
		build_goal2_cond1_True(spec);
	}
	
//	static public void testPrioritygoal1_cond1_over_goal2_cond1_Alerts(IBooleanSpecification spec) {
//		build_goal1_cond1_Alert_True(spec);
//		build_goal2_cond1_Alert_False(spec);
//	}
	
	static public void testPrioritygoal2_cond1_over_goal1_cond1_Alerts(IBooleanSpecification spec) {
		build_goal1_cond1_Alert_True(spec);
		build_goal2_cond1_Alert_True(spec);
	}
	
//	static public void testPrioritygoal2_cond1_over_goal3_cond1(IBooleanSpecification spec) {
//		build_goal2_cond1_True(spec);
//		build_goal3_cond1_False(spec);
//	}
//	
//	static public void testPrioritygoal3_cond1_over_goal2_cond1(IBooleanSpecification spec) {
//		build_goal2_cond1_False(spec);
//		build_goal3_cond1_True(spec);
//	}
//	
//	static public void testPrioritygoal2_cond1_over_goal3_cond1_Alerts(IBooleanSpecification spec) {
//		build_goal2_cond1_Alert_True(spec);
//		build_goal3_cond1_Alert_False(spec);
//	}
//	
//	static public void testPrioritygoal3_cond1_over_goal2_cond1_Alerts(IBooleanSpecification spec) {
//		build_goal2_cond1_Alert_False(spec);
//		build_goal3_cond1_Alert_True(spec);
//	}
	
//	static public void testPrioritygoal3_cond2_over_goal4_cond1(IBooleanSpecification spec) {
//		build_goal3_cond2_True(spec);
//		build_goal4_cond1_False(spec);
//	}
//	
//	static public void testPrioritygoal4_cond1_over_goal3_cond2(IBooleanSpecification spec) {
//		build_goal3_cond2_False(spec);
//		build_goal4_cond1_True(spec);
//	}
//	
//	static public void testPrioritygoal3_cond2_over_goal4_cond1_Alerts(IBooleanSpecification spec) {
//		build_goal3_cond2_Alert_True(spec);
//		build_goal4_cond1_Alert_False(spec);
//	}
//	
//	static public void testPrioritygoal4_cond1_over_goal3_cond2_Alerts(IBooleanSpecification spec) {
//		build_goal3_cond2_Alert_False(spec);
//		build_goal4_cond1_Alert_True(spec);
//	}
	
//test parallel rules by applying : test(!goalX_cond1 and !goalX_cond2)
	
	
	static public void testParallelgoal3_cond1_goal3_cond2(IBooleanSpecification spec) {
		build_goal3_cond2_True(spec);
		build_goal3_cond1_True(spec);
	}
	
	static public void testParallelgoal3_cond1_goal3_cond2_Alerts(IBooleanSpecification spec) {
		build_goal3_cond2_Alert_True(spec);
		build_goal3_cond1_Alert_True(spec);
	}
	
	
//	static public void testParallelgoal4_cond1_goal4_cond2(IBooleanSpecification spec) {
//		build_goal4_cond2_True(spec);
//		build_goal4_cond1_True(spec);
//	}
//	
//	static public void testParallelgoal4_cond1_goal4_cond2_Alerts(IBooleanSpecification spec) {
//		build_goal4_cond2_Alert_True(spec);
//		build_goal4_cond1_Alert_True(spec);
//	}
//	
//	static public void testParallelgoal4_cond2_goal4_cond3(IBooleanSpecification spec) {
//		build_goal4_cond3_True(spec);
//		build_goal4_cond2_True(spec);
//	}
//	
//	static public void testParallelgoal4_cond2_goal4_cond3_Alerts(IBooleanSpecification spec) {
//		build_goal4_cond3_Alert_True(spec);
//		build_goal4_cond2_Alert_True(spec);
//	}
//	
//	static public void testParallelgoal4_cond3_goal4_cond4(IBooleanSpecification spec) {
//		build_goal4_cond4_True(spec);
//		build_goal4_cond3_True(spec);
//	}
//	
//	static public void testParallelgoal4_cond3_goal4_cond4_Alerts(IBooleanSpecification spec) {
//		build_goal4_cond4_Alert_True(spec);
//		build_goal4_cond3_Alert_True(spec);
//	}
//	
//	static public void testParallelgoal4_cond4_goal4_cond5(IBooleanSpecification spec) {
//		build_goal4_cond5_True(spec);
//		build_goal4_cond4_True(spec);
//	}
//	
//	static public void testParallelgoal4_cond4_goal4_cond5_Alerts(IBooleanSpecification spec) {
//		build_goal4_cond5_Alert_True(spec);
//		build_goal4_cond4_Alert_True(spec);
//	}
	
	
//	static public void testParallelgoal1_cond1_goal2_cond1(IBooleanSpecification spec) {
//		build_goal1_cond1_True(spec);
//		build_goal2_cond1_True(spec);
//	}
//	
//	static public void testParallelgoal1_cond1_goal2_cond1_Alerts(IBooleanSpecification spec) {
//		build_goal1_cond1_Alert_True(spec);
//		build_goal2_cond1_Alert_True(spec);
//	}
	
	static public void testParallelgoal2_cond1_goal3_cond1(IBooleanSpecification spec) {
		build_goal2_cond1_True(spec);
		build_goal3_cond1_True(spec);
	}
	
	static public void testParallelgoal2_cond1_goal3_cond1_Alerts(IBooleanSpecification spec) {
		build_goal2_cond1_Alert_True(spec);
		build_goal3_cond1_Alert_True(spec);
	}
	
	static public void testParallelgoal3_cond2_goal4_cond1(IBooleanSpecification spec) {
		build_goal3_cond2_True(spec);
		build_goal4_cond1_True(spec);
	}
	
	static public void testParallelgoal3_cond2_goal4_cond1_Alerts(IBooleanSpecification spec) {
		build_goal3_cond2_Alert_True(spec);
		build_goal4_cond1_Alert_True(spec);
	}
	
}
