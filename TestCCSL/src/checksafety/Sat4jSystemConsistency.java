package checksafety;
import fr.kairos.timesquare.ccsl.sat.IBooleanSpecification;

public class Sat4jSystemConsistency{
	
	static public void system_solution(IBooleanSpecification spec){
		coherence_all_properties(spec);
		coherence_all_goals_behaviors(spec);
		spec.and("system_solution", "coherence_all_properties", "coherence_all_goals_behaviors");
	}
	
	static public void coherence_all_goals_behaviors(IBooleanSpecification spec){
		coherence_goal1(spec);
		coherence_goal2(spec);
		coherence_goal3(spec);
		coherence_goal4(spec);
		spec.or("coherence_all_goals_behaviors", "coherence_goal1","coherence_goal2",
				"coherence_goal3","coherence_goal4");
	}
	
	static public void coherence_all_goals_behaviors_true(IBooleanSpecification spec){
		coherence_all_goals_behaviors(spec);
		spec.forces("coherence_all_goals_behaviors");
	}
	
	static public void coherence_goal1(IBooleanSpecification spec){
		//Sat4jRulesConsistency.build_goal1_cond1_Alert(spec);
		Sat4jRules.build_goal1_cond1(spec);
		spec.or("coherence_goal1", "goal1_cond1");
	}
	
	static public void coherence_goal1_FALSE(IBooleanSpecification spec){
		//Sat4jRulesConsistency.build_goal1_cond1_Alert_False(spec);
		Sat4jRulesConsistency.build_goal1_cond1_False(spec);
	}
	
	static public void coherence_goal2(IBooleanSpecification spec){
		coherence_goal1_FALSE(spec);
		//Sat4jRulesConsistency.build_goal2_cond1_Alert(spec);
		Sat4jRules.build_goal2_cond1(spec);
		spec.or("coherence_goal2", "goal2_cond1");
	}
	
	static public void coherence_goal3(IBooleanSpecification spec){
		coherence_goal1_FALSE(spec);
		//Sat4jRulesConsistency.build_goal3_cond1_Alert(spec);
		//Sat4jRulesConsistency.build_goal3_cond2_Alert(spec);
		Sat4jRules.build_goal3_cond1(spec);
		Sat4jRules.build_goal3_cond2(spec);
		spec.or("coherence_goal3", "goal3_cond1","goal3_cond2");
		
	}
	
	static public void coherence_goal4(IBooleanSpecification spec){
		coherence_goal1_FALSE(spec);
		coherence_goal4_cond1(spec);
		coherence_goal4_cond2(spec);
		coherence_goal4_cond3(spec);
		coherence_goal4_cond4(spec);
		coherence_goal4_cond5(spec);
		spec.or("coherence_goal4", "goal4_cond1","goal4_cond2","goal4_cond3","goal4_cond4","goal4_cond5");
	}
	
	static public void coherence_goal4_cond1(IBooleanSpecification spec){
		//Sat4jRulesConsistency.build_goal4_cond1_Alert(spec);
		Sat4jRules.build_goal4_cond1(spec);
	}
	
	
	static public void coherence_goal4_cond1_FALSE(IBooleanSpecification spec){
		//Sat4jRulesConsistency.build_goal4_cond1_Alert_False(spec);
		Sat4jRulesConsistency.build_goal4_cond1_False(spec);
	}
	
	static public void coherence_goal4_cond2(IBooleanSpecification spec){
		coherence_goal4_cond1_FALSE(spec);
		//Sat4jRulesConsistency.build_goal4_cond2_Alert(spec);
		Sat4jRules.build_goal4_cond2(spec);
		
	}
	
	static public void coherence_goal4_cond2_FALSE(IBooleanSpecification spec){
		//Sat4jRulesConsistency.build_goal4_cond2_Alert_False(spec);
		Sat4jRulesConsistency.build_goal4_cond2_False(spec);
	}
	
	static public void coherence_goal4_cond3(IBooleanSpecification spec){
		coherence_goal4_cond2_FALSE(spec);
		//Sat4jRulesConsistency.build_goal4_cond3_Alert(spec);
		Sat4jRules.build_goal4_cond3(spec);
	}
	
	static public void coherence_goal4_cond3_FALSE(IBooleanSpecification spec){
		//Sat4jRulesConsistency.build_goal4_cond3_Alert_False(spec);
		Sat4jRulesConsistency.build_goal4_cond3_False(spec);
	}
	
	static public void coherence_goal4_cond4(IBooleanSpecification spec){
		coherence_goal4_cond3_FALSE(spec);
		//Sat4jRulesConsistency.build_goal4_cond4_Alert(spec);
		Sat4jRules.build_goal4_cond4(spec);
	}
	
	static public void coherence_goal4_cond4_FALSE(IBooleanSpecification spec){
		//Sat4jRulesConsistency.build_goal4_cond4_Alert_False(spec);
		Sat4jRulesConsistency.build_goal4_cond4_False(spec);
	}
	
	static public void coherence_goal4_cond5(IBooleanSpecification spec){
		coherence_goal4_cond4_FALSE(spec);
		//Sat4jRulesConsistency.build_goal4_cond5_Alert(spec);
		Sat4jRules.build_goal4_cond5(spec);
	}
	
	
	static public void coherence_all_properties(IBooleanSpecification spec){
		coherence_traffic_jam(spec);
		coherence_front_car_distance(spec);
		coherence_front_car_tracking(spec);
		coherence_line_detection(spec);
		coherence_straddling_car_distance(spec);
		coherence_straddling_car_tracking(spec);
		coherence_stable_control(spec);
		coherence_rear_car_distance(spec);
		spec.or("coherence_all_properties",  "coherence_traffic_jam",
				"coherence_front_car_distance", "coherence_front_car_tracking",
				"coherence_line_detection", "coherence_straddling_car_distance",
				"coherence_straddling_car_tracking", "coherence_stable_control",
				"coherence_rear_car_distance");
	}
		
	static public void coherence_traffic_jam(IBooleanSpecification spec){
		spec.clause("yes_traffic_jam", "no_traffic_jam","coherence_traffic_jam");
		spec.clause("no_traffic_jam", "yes_traffic_jam","coherence_traffic_jam");
		spec.clause("coherence_traffic_jam", "yes_traffic_jam", "no_traffic_jam");
		spec.forbids("yes_traffic_jam","no_traffic_jam");
		spec.forbids("no_traffic_jam","yes_traffic_jam");
		spec.not("coherence_traffic_jam");
	}
	
		
	static public void coherence_front_car_distance(IBooleanSpecification spec){
		spec.clause("not_exist_front_car_distance", "safe_distance_front_car_distance","acc_distance_front_car_distance","strong_braking_distance_front_car_distance","imminent_collision_distance_front_car_distance","coherence_front_car_distance");
		spec.clause("safe_distance_front_car_distance", "not_exist_front_car_distance","acc_distance_front_car_distance","strong_braking_distance_front_car_distance","imminent_collision_distance_front_car_distance","coherence_front_car_distance");
		spec.clause("acc_distance_front_car_distance", "not_exist_front_car_distance","safe_distance_front_car_distance","strong_braking_distance_front_car_distance","imminent_collision_distance_front_car_distance","coherence_front_car_distance");
		spec.clause("strong_braking_distance_front_car_distance", "not_exist_front_car_distance","safe_distance_front_car_distance","acc_distance_front_car_distance","imminent_collision_distance_front_car_distance","coherence_front_car_distance");
		spec.clause("imminent_collision_distance_front_car_distance", "not_exist_front_car_distance","safe_distance_front_car_distance","acc_distance_front_car_distance","strong_braking_distance_front_car_distance","coherence_front_car_distance");
		spec.clause("coherence_front_car_distance", "not_exist_front_car_distance", "safe_distance_front_car_distance", "acc_distance_front_car_distance", "strong_braking_distance_front_car_distance", "imminent_collision_distance_front_car_distance");
		spec.forbids("not_exist_front_car_distance","safe_distance_front_car_distance");
		spec.forbids("not_exist_front_car_distance","acc_distance_front_car_distance");
		spec.forbids("not_exist_front_car_distance","strong_braking_distance_front_car_distance");
		spec.forbids("not_exist_front_car_distance","imminent_collision_distance_front_car_distance");
		spec.forbids("safe_distance_front_car_distance","not_exist_front_car_distance");
		spec.forbids("safe_distance_front_car_distance","acc_distance_front_car_distance");
		spec.forbids("safe_distance_front_car_distance","strong_braking_distance_front_car_distance");
		spec.forbids("safe_distance_front_car_distance","imminent_collision_distance_front_car_distance");
		spec.forbids("acc_distance_front_car_distance","not_exist_front_car_distance");
		spec.forbids("acc_distance_front_car_distance","safe_distance_front_car_distance");
		spec.forbids("acc_distance_front_car_distance","strong_braking_distance_front_car_distance");
		spec.forbids("acc_distance_front_car_distance","imminent_collision_distance_front_car_distance");
		spec.forbids("strong_braking_distance_front_car_distance","not_exist_front_car_distance");
		spec.forbids("strong_braking_distance_front_car_distance","safe_distance_front_car_distance");
		spec.forbids("strong_braking_distance_front_car_distance","acc_distance_front_car_distance");
		spec.forbids("strong_braking_distance_front_car_distance","imminent_collision_distance_front_car_distance");
		spec.forbids("imminent_collision_distance_front_car_distance","not_exist_front_car_distance");
		spec.forbids("imminent_collision_distance_front_car_distance","safe_distance_front_car_distance");
		spec.forbids("imminent_collision_distance_front_car_distance","acc_distance_front_car_distance");
		spec.forbids("imminent_collision_distance_front_car_distance","strong_braking_distance_front_car_distance");
		spec.not("coherence_front_car_distance");
	}
	
		
	static public void coherence_front_car_tracking(IBooleanSpecification spec){
		spec.clause("not_confirmed_front_car_tracking", "not_exist_front_car_tracking","disappeared_less_than_t1_front_car_tracking","disappeared_more_than_t1_front_car_tracking","stable_tracking_front_car_tracking","coherence_front_car_tracking");
		spec.clause("not_exist_front_car_tracking", "not_confirmed_front_car_tracking","disappeared_less_than_t1_front_car_tracking","disappeared_more_than_t1_front_car_tracking","stable_tracking_front_car_tracking","coherence_front_car_tracking");
		spec.clause("disappeared_less_than_t1_front_car_tracking", "not_confirmed_front_car_tracking","not_exist_front_car_tracking","disappeared_more_than_t1_front_car_tracking","stable_tracking_front_car_tracking","coherence_front_car_tracking");
		spec.clause("disappeared_more_than_t1_front_car_tracking", "not_confirmed_front_car_tracking","not_exist_front_car_tracking","disappeared_less_than_t1_front_car_tracking","stable_tracking_front_car_tracking","coherence_front_car_tracking");
		spec.clause("stable_tracking_front_car_tracking", "not_confirmed_front_car_tracking","not_exist_front_car_tracking","disappeared_less_than_t1_front_car_tracking","disappeared_more_than_t1_front_car_tracking","coherence_front_car_tracking");
		spec.clause("coherence_front_car_tracking", "not_confirmed_front_car_tracking", "not_exist_front_car_tracking", "disappeared_less_than_t1_front_car_tracking", "disappeared_more_than_t1_front_car_tracking", "stable_tracking_front_car_tracking");
		spec.forbids("not_confirmed_front_car_tracking","not_exist_front_car_tracking");
		spec.forbids("not_confirmed_front_car_tracking","disappeared_less_than_t1_front_car_tracking");
		spec.forbids("not_confirmed_front_car_tracking","disappeared_more_than_t1_front_car_tracking");
		spec.forbids("not_confirmed_front_car_tracking","stable_tracking_front_car_tracking");
		spec.forbids("not_exist_front_car_tracking","not_confirmed_front_car_tracking");
		spec.forbids("not_exist_front_car_tracking","disappeared_less_than_t1_front_car_tracking");
		spec.forbids("not_exist_front_car_tracking","disappeared_more_than_t1_front_car_tracking");
		spec.forbids("not_exist_front_car_tracking","stable_tracking_front_car_tracking");
		spec.forbids("disappeared_less_than_t1_front_car_tracking","not_confirmed_front_car_tracking");
		spec.forbids("disappeared_less_than_t1_front_car_tracking","not_exist_front_car_tracking");
		spec.forbids("disappeared_less_than_t1_front_car_tracking","disappeared_more_than_t1_front_car_tracking");
		spec.forbids("disappeared_less_than_t1_front_car_tracking","stable_tracking_front_car_tracking");
		spec.forbids("disappeared_more_than_t1_front_car_tracking","not_confirmed_front_car_tracking");
		spec.forbids("disappeared_more_than_t1_front_car_tracking","not_exist_front_car_tracking");
		spec.forbids("disappeared_more_than_t1_front_car_tracking","disappeared_less_than_t1_front_car_tracking");
		spec.forbids("disappeared_more_than_t1_front_car_tracking","stable_tracking_front_car_tracking");
		spec.forbids("stable_tracking_front_car_tracking","not_confirmed_front_car_tracking");
		spec.forbids("stable_tracking_front_car_tracking","not_exist_front_car_tracking");
		spec.forbids("stable_tracking_front_car_tracking","disappeared_less_than_t1_front_car_tracking");
		spec.forbids("stable_tracking_front_car_tracking","disappeared_more_than_t1_front_car_tracking");
		spec.not("coherence_front_car_tracking");
	}
	
		
	static public void coherence_line_detection(IBooleanSpecification spec){
		spec.clause("stable_line_detection", "no_detection_in_less_than_t6_line_detection","no_detection_in_more_than_t6_line_detection","coherence_line_detection");
		spec.clause("no_detection_in_less_than_t6_line_detection", "stable_line_detection","no_detection_in_more_than_t6_line_detection","coherence_line_detection");
		spec.clause("no_detection_in_more_than_t6_line_detection", "stable_line_detection","no_detection_in_less_than_t6_line_detection","coherence_line_detection");
		spec.clause("coherence_line_detection", "stable_line_detection", "no_detection_in_less_than_t6_line_detection", "no_detection_in_more_than_t6_line_detection");
		spec.forbids("stable_line_detection","no_detection_in_less_than_t6_line_detection");
		spec.forbids("stable_line_detection","no_detection_in_more_than_t6_line_detection");
		spec.forbids("no_detection_in_less_than_t6_line_detection","stable_line_detection");
		spec.forbids("no_detection_in_less_than_t6_line_detection","no_detection_in_more_than_t6_line_detection");
		spec.forbids("no_detection_in_more_than_t6_line_detection","stable_line_detection");
		spec.forbids("no_detection_in_more_than_t6_line_detection","no_detection_in_less_than_t6_line_detection");
		spec.not("coherence_line_detection");
	}
	
		
	static public void coherence_straddling_car_distance(IBooleanSpecification spec){
		spec.clause("imminent_collision_distance_straddling_car_distance", "safe_distance_straddling_car_distance","acc_distance_straddling_car_distance","strong_braking_distance_straddling_car_distance","not_exist_straddling_car_distance","coherence_straddling_car_distance");
		spec.clause("safe_distance_straddling_car_distance", "imminent_collision_distance_straddling_car_distance","acc_distance_straddling_car_distance","strong_braking_distance_straddling_car_distance","not_exist_straddling_car_distance","coherence_straddling_car_distance");
		spec.clause("acc_distance_straddling_car_distance", "imminent_collision_distance_straddling_car_distance","safe_distance_straddling_car_distance","strong_braking_distance_straddling_car_distance","not_exist_straddling_car_distance","coherence_straddling_car_distance");
		spec.clause("strong_braking_distance_straddling_car_distance", "imminent_collision_distance_straddling_car_distance","safe_distance_straddling_car_distance","acc_distance_straddling_car_distance","not_exist_straddling_car_distance","coherence_straddling_car_distance");
		spec.clause("not_exist_straddling_car_distance", "imminent_collision_distance_straddling_car_distance","safe_distance_straddling_car_distance","acc_distance_straddling_car_distance","strong_braking_distance_straddling_car_distance","coherence_straddling_car_distance");
		spec.clause("coherence_straddling_car_distance", "imminent_collision_distance_straddling_car_distance", "safe_distance_straddling_car_distance", "acc_distance_straddling_car_distance", "strong_braking_distance_straddling_car_distance", "not_exist_straddling_car_distance");
		spec.forbids("imminent_collision_distance_straddling_car_distance","safe_distance_straddling_car_distance");
		spec.forbids("imminent_collision_distance_straddling_car_distance","acc_distance_straddling_car_distance");
		spec.forbids("imminent_collision_distance_straddling_car_distance","strong_braking_distance_straddling_car_distance");
		spec.forbids("imminent_collision_distance_straddling_car_distance","not_exist_straddling_car_distance");
		spec.forbids("safe_distance_straddling_car_distance","imminent_collision_distance_straddling_car_distance");
		spec.forbids("safe_distance_straddling_car_distance","acc_distance_straddling_car_distance");
		spec.forbids("safe_distance_straddling_car_distance","strong_braking_distance_straddling_car_distance");
		spec.forbids("safe_distance_straddling_car_distance","not_exist_straddling_car_distance");
		spec.forbids("acc_distance_straddling_car_distance","imminent_collision_distance_straddling_car_distance");
		spec.forbids("acc_distance_straddling_car_distance","safe_distance_straddling_car_distance");
		spec.forbids("acc_distance_straddling_car_distance","strong_braking_distance_straddling_car_distance");
		spec.forbids("acc_distance_straddling_car_distance","not_exist_straddling_car_distance");
		spec.forbids("strong_braking_distance_straddling_car_distance","imminent_collision_distance_straddling_car_distance");
		spec.forbids("strong_braking_distance_straddling_car_distance","safe_distance_straddling_car_distance");
		spec.forbids("strong_braking_distance_straddling_car_distance","acc_distance_straddling_car_distance");
		spec.forbids("strong_braking_distance_straddling_car_distance","not_exist_straddling_car_distance");
		spec.forbids("not_exist_straddling_car_distance","imminent_collision_distance_straddling_car_distance");
		spec.forbids("not_exist_straddling_car_distance","safe_distance_straddling_car_distance");
		spec.forbids("not_exist_straddling_car_distance","acc_distance_straddling_car_distance");
		spec.forbids("not_exist_straddling_car_distance","strong_braking_distance_straddling_car_distance");
		spec.not("coherence_straddling_car_distance");
	}
	
		
	static public void coherence_straddling_car_tracking(IBooleanSpecification spec){
		spec.clause("not_exist_straddling_car_tracking", "straddling_less_than_t7_straddling_car_tracking","straddling_more_than_t7_straddling_car_tracking","coherence_straddling_car_tracking");
		spec.clause("straddling_less_than_t7_straddling_car_tracking", "not_exist_straddling_car_tracking","straddling_more_than_t7_straddling_car_tracking","coherence_straddling_car_tracking");
		spec.clause("straddling_more_than_t7_straddling_car_tracking", "not_exist_straddling_car_tracking","straddling_less_than_t7_straddling_car_tracking","coherence_straddling_car_tracking");
		spec.clause("coherence_straddling_car_tracking", "not_exist_straddling_car_tracking", "straddling_less_than_t7_straddling_car_tracking", "straddling_more_than_t7_straddling_car_tracking");
		spec.forbids("not_exist_straddling_car_tracking","straddling_less_than_t7_straddling_car_tracking");
		spec.forbids("not_exist_straddling_car_tracking","straddling_more_than_t7_straddling_car_tracking");
		spec.forbids("straddling_less_than_t7_straddling_car_tracking","not_exist_straddling_car_tracking");
		spec.forbids("straddling_less_than_t7_straddling_car_tracking","straddling_more_than_t7_straddling_car_tracking");
		spec.forbids("straddling_more_than_t7_straddling_car_tracking","not_exist_straddling_car_tracking");
		spec.forbids("straddling_more_than_t7_straddling_car_tracking","straddling_less_than_t7_straddling_car_tracking");
		spec.not("coherence_straddling_car_tracking");
	}
	
		
	static public void coherence_stable_control(IBooleanSpecification spec){
		spec.clause("stable_stable_control", "not_stable_stable_control","coherence_stable_control");
		spec.clause("not_stable_stable_control", "stable_stable_control","coherence_stable_control");
		spec.clause("coherence_stable_control", "stable_stable_control", "not_stable_stable_control");
		spec.forbids("stable_stable_control","not_stable_stable_control");
		spec.forbids("not_stable_stable_control","stable_stable_control");
		spec.not("coherence_stable_control");
	}
	
		
	static public void coherence_rear_car_distance(IBooleanSpecification spec){
		spec.clause("not_exist_rear_car_distance", "safe_distance_rear_car_distance","emergency_distance_rear_car_distance","coherence_rear_car_distance");
		spec.clause("safe_distance_rear_car_distance", "not_exist_rear_car_distance","emergency_distance_rear_car_distance","coherence_rear_car_distance");
		spec.clause("emergency_distance_rear_car_distance", "not_exist_rear_car_distance","safe_distance_rear_car_distance","coherence_rear_car_distance");
		spec.clause("coherence_rear_car_distance", "not_exist_rear_car_distance", "safe_distance_rear_car_distance", "emergency_distance_rear_car_distance");
		spec.forbids("not_exist_rear_car_distance","safe_distance_rear_car_distance");
		spec.forbids("not_exist_rear_car_distance","emergency_distance_rear_car_distance");
		spec.forbids("safe_distance_rear_car_distance","not_exist_rear_car_distance");
		spec.forbids("safe_distance_rear_car_distance","emergency_distance_rear_car_distance");
		spec.forbids("emergency_distance_rear_car_distance","not_exist_rear_car_distance");
		spec.forbids("emergency_distance_rear_car_distance","safe_distance_rear_car_distance");
		spec.not("coherence_rear_car_distance");
	}
	
}
