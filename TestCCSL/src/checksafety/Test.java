package checksafety;

import java.util.List;

import fr.kairos.lightccsl.core.stepper.ClockStatus;
import fr.kairos.lightccsl.core.stepper.ISolutionSet;
import fr.kairos.lightccsl.core.stepper.IStep;
import fr.kairos.timesquare.ccsl.sat.IBooleanSpecification;
import fr.kairos.timesquare.ccsl.sat.ISATSolverBuilder;
import fr.unice.lightccsl.sat.SAT4JSolverBuilder;

public class Test {
	public static void main(String[] args) {
		ISATSolverBuilder builder = new SAT4JSolverBuilder();	
//		ISATSolverBuilder builder = new SATBDDBuilder();
		IBooleanSpecification boolSpec = builder.specification();
//		BooleanSatisfactionProblem bsp = (BooleanSatisfactionProblem)boolSpec;
		
		MyNameMapper mapper = new MyNameMapper();
		boolSpec.setNameMapper(mapper); 
 
		//Sat4jRules.build_goal2_cond1(boolSpec);
		//Sat4jRulesConsistency.testParallelgoal2_cond1_goal3_cond1(boolSpec);
		//Sat4jSystemConsistency.coherence_front_car_tracking(boolSpec);
		//Sat4jSystemConsistency.coherence_straddling_car_tracking(boolSpec);
		//Sat4jSystemConsistency.coherence_straddling_car_tracking(boolSpec);
		//Sat4jSystemConsistency.coherence_goal2(boolSpec);
		Sat4jSystemConsistency.system_solution(boolSpec);

		
		//	  ISolutionSet set = builder.solution();
		//	  try {
		//		IStep step = set.pickOneSolution();
		//		for (int i = 0; i< step.size(); i++) {
		//			System.out.print(mapper.getNameFromId(i) + ":");
		//			ClockStatus status = step.status(i);
		//			switch(status) {
		//			case Must: System.out.println("1, "); break;
		//			case Cannot: System.out.println("0, "); break;
		//			case May: System.out.println("?, "); break;
		//			case Undetermined: System.out.println("x, "); break;
		//			}
		//		}
		//		System.out.println(step);


		ISolutionSet set = builder.solution();

		try {
			List<? extends IStep> step = set.allSolutions();
			System.out.print(step+"\n");
			for (int j = 0; j<= step.size()-1; j++) {
				System.out.println("Solution "+(j +1)+":");
				System.out.println(step.get(j));	
				IStep onestep = step.get(j);
				for (int i = 0; i< onestep.size(); i++) {
					System.out.print((i +1)+": " + mapper.getNameFromId(i) + "=");
					ClockStatus status = onestep.status(i);
					switch(status) {
					case Must: System.out.println("1, "); break;
					case Cannot: System.out.println("0, "); break;
					case May: System.out.println("?, "); break;
					case Undetermined: System.out.println("x, "); break;
					}
				}
				System.out.println("\n");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
